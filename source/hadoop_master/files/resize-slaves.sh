#!/bin/bash

SLAVES_FILE=$HADOOP_INSTALL/etc/hadoop/slaves

echo "SLAVES="$SLAVES
if [[ -n $SLAVES ]]
then
    echo "Slaves number is $SLAVES, change the Hadoop slaves file..."
    # if SLAVES env variable change the $HADOOP_INSTALL/etc/hadoop/slaves file
    echo "master.yaolun.com" > $SLAVES_FILE
    i=1
    while [ $i -lt $SLAVES ]
    do
        # add slaves hostname to slaves file
        echo "slave$i.yaolun.com" >> $SLAVES_FILE
        ((i++))
    done 
fi

/bin/bash /root/start-ssh-serf.sh

exit 0

