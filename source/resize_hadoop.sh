#!/bin/bash

# $1 is the node number of the hadoop cluster
num=$1

# check argument > 0
if [ $# = 0 ]; then
	echo "Usage: sudo resize_hadoop.sh <num>"
	exit 1
fi

cd hadoop_master

# change the slaves file
echo "master.yaolun.com" > files/slaves
i=1
while [ $i -lt $num ]
do
    # add slaves hostname to slaves file
	echo "slave$i.yaolun.com" >> files/slaves
	((i++))
done 

# delete master container
sudo docker rm -f master 

# delete hadoop_master image
sudo docker rmi yaolunlun/hadoop-master

# rebuild hadoop_master image
pwd
sudo docker build -t yaolunlun/hadoop-master .

exit 0

