#!/bin/bash
#
# update debian image
#

ver=stable
name=debian:$ver

echo -e "\n\nsudo docker rmi yaolunlun/$name"
sudo docker rmi yaolunlun/$name

cd $1
echo -e "\n\nsudo docker build -t yaolunlun/$name ."
/usr/bin/time -f "real  %e" sudo docker build -t yaolunlun/$name ./
cd ..

echo -e "\n----------old images"
cat images.old; rm images.old

echo -e "\n----------new images"
sudo docker images

exit 0

